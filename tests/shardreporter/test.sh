#!/bin/bash

set -xe
set -o pipefail

# wait for all solrs to be up
while ! nc -z solr-1 8983; do
    echo "Solr is not yet up"
    sleep 1
done
while ! nc -z solr-2 8983; do
    echo "Solr is not yet up"
    sleep 1
done
while ! nc -z solr-3 8983; do
    echo "Solr is not yet up"
    sleep 1
done
sleep 5


# Create our test collection
curl 'http://solr-1:8983/solr/admin/collections?action=CREATE&name=test&numShards=1&collection.configName=_default&replicationFactor=3'
# Wait for solr to finish
sleep 5
# Issue a query
curl 'http://solr-1:8983/solr/test/select?q=*%3A*'
# sleep (enough for shardreporter to run)
sleep 15
# Get the metrics API on each node
# The leader will stack trace
wget --content-on-error -qO- http://solr-1:8983/solr/admin/metrics  | jq -r '.error.trace'
wget --content-on-error -qO- http://solr-2:8983/solr/admin/metrics  | jq -r '.error.trace'
wget --content-on-error -qO- http://solr-3:8983/solr/admin/metrics  | jq -r '.error.trace'
