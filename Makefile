clean:
	docker-compose \
	  -f docker-compose.yml \
	  -f tests/shardreporter/docker-compose.override.yml \
	  down

build:
	docker-compose \
	  -f docker-compose.yml \
	  -f tests/shardreporter/docker-compose.override.yml \
	  build

test: clean build
	docker-compose \
	  -f docker-compose.yml \
	  -f tests/shardreporter/docker-compose.override.yml \
	  up --exit-code-from test \
	  --abort-on-container-exit
